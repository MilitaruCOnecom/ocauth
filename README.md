# OCAuth

- - - -

### Installation
````
    composer require militaruc/ocauth
````

##### artisan publish all from OCAuth package ( this will overwrite config.auth.php, app/User.php, ...)
    
````
    php artisan vendor:publish --provider="Militaruc\Ocauth\App\Providers\OcauthServiceProvider" --force
````
##### OR artisan publish by tags:

* migration
* seeds
* views

````
    php artisan vendor:publish --provider="Militaruc\Ocauth\App\Providers\OcauthServiceProvider" --tag=migrations --force
````

##### Run migration:
````
    php artisan migrate --seed OR php artisan migrate:refresh --seed
````

- - - -

#####   Content
*   Multi auth
    1. Admins
    2.  Users
    2.1. Regular registration + login
    2.2. Socialite registration + login
        
#####   .env file ( replace this credentials with your own for each provider )
````
    GOOGLE_CLIENT_ID="Your Client ID"
    GOOGLE_APP_SECRET="Your App Secret"
    GOOGLE_REDIRECT="Your Callback Redirect"
````

After vendor:publish enable your socialite providers in /app/ocauthsocials.php

