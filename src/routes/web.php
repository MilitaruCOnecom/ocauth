<?php

Route::group(['middleware' => ['web']], function () {

    Route::get('home', function(){ return view('home'); })->name('home');

    Route::get('/login', 'Militaruc\Ocauth\App\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Militaruc\Ocauth\App\Http\Controllers\Auth\LoginController@login')->name('login.submit');
    Route::post('/logout', 'Militaruc\Ocauth\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

    Route::get('/register', 'Militaruc\Ocauth\App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'Militaruc\Ocauth\App\Http\Controllers\Auth\RegisterController@register');

    Route::get('/auth/{provider}', 'Militaruc\Ocauth\App\Http\Controllers\Auth\LoginController@redirectToProvider');
    Route::get('/callback/{provider}', 'Militaruc\Ocauth\App\Http\Controllers\Auth\LoginController@handleProviderCallback');

    Route::post('/password/email', 'Militaruc\Ocauth\App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/request', 'Militaruc\Ocauth\App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/reset', 'Militaruc\Ocauth\App\Http\Controllers\Auth\ResetPasswordController@reset');
    Route::post('/password/reset/{token}', 'Militaruc\Ocauth\App\Http\Controllers\Auth\ResetPasswordController@showResetForm')->name('password.reset');
});

Route::group(['middleware' => ['admin']], function () {

    Route::prefix('admin')->group(function () {

        Route::get('/login', 'Militaruc\Ocauth\App\Http\Controllers\Auth\AdminLoginController@showLoginForm')->name('admin.login');
        Route::post('/login', 'Militaruc\Ocauth\App\Http\Controllers\Auth\AdminLoginController@login')->name('admin.login.submit');
        Route::post('/logout', 'Militaruc\Ocauth\App\Http\Controllers\Auth\AdminLoginController@logout')->name('admin.logout');

//        Route::post('/password/email', '\App\Http\Controllers\Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
//        Route::get('/password/reset', '\App\Http\Controllers\Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
//        Route::post('/password/reset', '\App\Http\Controllers\Auth\AdminResetPasswordController@reset');
//        Route::get('/password/reset/{token}', '\App\Http\Controllers\Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');

        Route::get('/', 'Militaruc\Ocauth\App\Http\Controllers\AdminController@index')->name('admin.index');

    });

});