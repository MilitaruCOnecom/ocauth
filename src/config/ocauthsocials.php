<?php

/**
 * On activation please provide credentials in your .env and app/services.php
 */

return [

    'google' => true,
    'facebook' => false,

];
