<?php

/**
 * Redirects after authenticated
 */
return [

    'redirects' => [
        'web' => 'home', // route name
        'admin' => 'admin.index', // route name
    ]

];
