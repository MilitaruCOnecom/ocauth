<?php

namespace Militaruc\Ocauth\App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectAuthenticatedAdmin
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        //dd(Auth::user());
        //dd('Register ADMIN Middleware');
        switch ($guard) {
            case 'admin':
                if (Auth::guard($guard)->check()) {
                    return redirect()->route(config('ocauth.redirects.admin'));
                }
                break;

            default:
                if (Auth::guard($guard)->check()) {
                    return redirect('/admin');
                }
                break;
        }
        //dd('Register ADMIN Middleware');
        //return redirect('/admin/login');

        return $next($request);
    }
}
